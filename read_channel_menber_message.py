from telethon import TelegramClient, connection
import logging
from telethon import sync, TelegramClient, events
from telethon.tl.functions.messages import GetDialogsRequest
from telethon.tl.types import InputPeerEmpty, UserStatusOffline, UserStatusRecently, UserStatusLastMonth, \
    UserStatusLastWeek
from telethon.tl.types import ChannelParticipantsSearch, InputPhoneContact
from telethon.tl.functions.channels import GetParticipantsRequest
from telethon.tl.functions.contacts import ImportContactsRequest
from telethon.tl.types import InputPeerChannel
from telethon.tl.types import InputPeerUser
from telethon.tl.functions.channels import InviteToChannelRequest
from telethon.tl.types import ChannelParticipantsSearch
from telethon.tl.functions.channels import GetParticipantsRequest
from telethon.tl.functions.messages import GetHistoryRequest
import json
from datetime import datetime, timedelta
import os

logging.basicConfig(level=logging.WARNING)

def get_start(phone, api_id, api_hash, target_group_link):
    folder_session = 'session/'
    client = TelegramClient(folder_session + phone, api_id, api_hash)
    client.connect()
    if not client.is_user_authorized():
        print('Login fail, need to run init_session')
    else:
        read_message_group(client, target_group_link)


def read_message_group(client, target_group_link):
    print('getting data start')
    root_path = os.path.dirname(os.path.abspath(__file__))
    path_contact_user = root_path + '/data/user/contacts_list.json'
    
    idGroup = 1695764096
    acHash = -4068853958814960025
    chanel = InputPeerChannel(idGroup, acHash)
    group_id = str(idGroup)
    offset = 0
    while_condition = True
    path_file = 'data/user/' + phone + "_" + group_id + '.json'

    listUser = []
    all_messages = []
    i=0;
    j=20000;
    limit=100;
    while while_condition:
        posts = client(GetHistoryRequest(
                peer=chanel,
                limit=limit,
                offset_date=None,
                offset_id=0,
                max_id=0,
                min_id=0,
                add_offset=offset,
                hash=0))
        all_messages.extend(posts.messages)
        offset += len(posts.messages)
        i = i+1
        print("length message")
        print(len(posts.messages))
        if len(posts.messages) < 1 :
            while_condition = False
        elif (i*limit) >= j :
            while_condition = False
        print("offset:")
        print(offset)
     # print(posts)
    for message in all_messages:
        if(message.from_id):
            listUser.append(message.from_id.user_id)
            # user = client.get_entity(message.from_id.user_id)
            # print(user)

    test_list = list(set(listUser))
    print ("The list after removing duplicates : "
        + str(test_list))
    with open(path_file, 'w', encoding='utf-8') as f:
        json.dump(test_list, f, indent=4, ensure_ascii=False)



#############
#start run

with open('config.json', 'r', encoding='utf-8') as f:
    config = json.loads(f.read())

accounts = config['accounts']
target_group_link = config['group_target_link']
folder_session = 'session/'

for account in accounts:
    api_id = account['api_id']
    api_hash = account['api_hash']
    phone = account['phone']
    get_start(phone, api_id, api_hash, target_group_link)
