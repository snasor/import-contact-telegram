# import-contact-into-telegram-group
Use `python 3` to add member from Group A to Group B (migrate members of your group)


## Require
* Environment of python 3 (Linux, Window)
* Need about 20 accounts to run (Switches accounts automatically when blocked)
* Each account needs to be in Source Group and Target Group
* Notice your region phone
* Your group must be a Super group

https://www.wikihow.com/Convert-a-Telegram-Group-to-a-Supergroup-on-PC-or-Mac

![Supper group](images/note_tele.png)
![Upgraded Supper group](images/note_tele2.png)

## Guide line

* Step 1: Install package `telethon`
```
pip install telethon
```

* Step 2: Create file config.json
Copy file config.json from config.example.json

```
{
	"group_target_link": "https://t.me/+.........",
	"accounts": [ --> array account
		{
			"phone": "+84XXXX",
			"api_id": 1234566,
			"api_hash": "57c6f3c72c2f21676d53be2eXXXXXX"
		}
	]
}
```

`accounts`: list your Telegram accounts; and for each accounts/phone, create an app in https://my.telegram.org/apps and copy the `api_id` and  `api_hash` into the config file.

`group_target_link` : copy the link from your telegram public group

* Step 3: After setting up your `config.json` file, run `python init_session.py`, enter phone and the code you received

![Init session](images/step1.png)

* Step 4: create `contacts_list.json` save file in folder `data/user`
```
[
    {
        "phone": "+855........",
        "lastname": ".......",
        "firstname": "......."
    }
]
```
* Step 5: run `python check_channel.py` to import contacts_list into group

Done!

## Ps: 
This repo is now actively being maintained and updated by:
snasor, south1907 and DanielTheGeek.

Create a new issue if you have legit issues and we will do our best to resolve them.

# youtube

https://youtu.be/38tn_whOt_Y?si=UzaH2pMPp87Ar24Z