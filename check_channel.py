from telethon import TelegramClient, connection
import logging
from telethon import sync, TelegramClient, events
from telethon.tl.functions.messages import GetDialogsRequest
from telethon.tl.types import InputPeerEmpty, UserStatusOffline, UserStatusRecently, UserStatusLastMonth, \
    UserStatusLastWeek
from telethon.tl.types import ChannelParticipantsSearch, InputPhoneContact
from telethon.tl.functions.channels import GetParticipantsRequest
from telethon.tl.functions.contacts import ImportContactsRequest
from telethon.tl.types import InputPeerChannel
from telethon.tl.types import InputPeerUser
from telethon.tl.functions.channels import InviteToChannelRequest
import json
from datetime import datetime, timedelta
import os

logging.basicConfig(level=logging.WARNING)


def get_start(phone, api_id, api_hash, target_group_link):
    folder_session = 'session/'
    client = TelegramClient(folder_session + phone, api_id, api_hash)
    client.connect()
    if not client.is_user_authorized():
        print('Login fail, need to run init_session')
    else:
        import_data_group(client, target_group_link)


def import_data_group(client, target_group_link):
    print('getting data start')
    root_path = os.path.dirname(os.path.abspath(__file__))
    path_contact_user = root_path + '/data/user/contacts_list.json'
    user_to_adds = []
    user_to_fails = []
    group_contact = []
    telegram_group_contact = []
    chanel = client.get_entity(target_group_link)
    target_group_entity = InputPeerChannel(chanel.id, chanel.access_hash)
    ####################
    #read contact json
    with open(path_contact_user, 'r', encoding='utf-8') as f:
        contactPhones = json.loads(f.read())
    
    for contactPhone in contactPhones:
        try:
            group_contact.append(InputPhoneContact(client_id=0, phone=contactPhone['phone'], first_name=contactPhone['firstname'], last_name=contactPhone['lastname']))
        except ValueError:
            print(ValueError)
    result = client(ImportContactsRequest(group_contact))
    print(len(result.users))
    for realContact in result.users:
        #print(realContact)
        tmp = {
            'id': realContact.id,
            'lastname': str(realContact.last_name),
            'firstname': str(realContact.first_name),
            'phone': str(realContact.phone),
        }
        telegram_group_contact.append(tmp)
        user_to_adds.append(InputPeerUser(realContact.id, realContact.access_hash))
    
    print('save telegram json')
    with open('data/user/result.json', 'w', encoding='utf-8') as f:
        json.dump(telegram_group_contact, f, indent=4, ensure_ascii=False)
    ########################        
    #add to group telegram
    print('add to group telegram')
    client(InviteToChannelRequest(target_group_entity, user_to_adds))

    #print(user_to_adds)
    # if len(result.users) == 0:
    #     user_to_fails.append(contactPhone)
    #     print('user not found' + '('+contactPhone['lastname']+' ' + contactPhone['firstname'] +') ' +contactPhone['phone'])
    # else:
    #     user_to_adds.append(InputPeerUser(result.users[0].id, result.users[0].access_hash))
    # for contactPhone in contactPhones:
    #     try:
    #         user = client.get_entity(contactPhone['phone'])
    #         user_to_adds.append(InputPeerUser(user.id, user.access_hash))
    #     except ValueError:
    #         contact = InputPhoneContact(client_id=0, phone=contactPhone['phone'], first_name=contactPhone['firstname'], last_name=contactPhone['lastname'])
    #         result = client(ImportContactsRequest([contact]))
    #         if len(result.users) == 0:
    #             user_to_fails.append(contactPhone)
    #             print('user not found' + '('+contactPhone['lastname']+' ' + contactPhone['firstname'] +') ' +contactPhone['phone'])
    #         else:
    #             user_to_adds.append(InputPeerUser(result.users[0].id, result.users[0].access_hash))

    ########################        
    #add to group telegram
    #client(InviteToChannelRequest(target_group_entity, user_to_adds))


with open('config.json', 'r', encoding='utf-8') as f:
    config = json.loads(f.read())

accounts = config['accounts']

target_group_link = config['group_target_link']

folder_session = 'session/'

for account in accounts:
    api_id = account['api_id']
    api_hash = account['api_hash']
    phone = account['phone']
    get_start(phone, api_id, api_hash, target_group_link)
